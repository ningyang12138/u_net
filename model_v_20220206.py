import torch
from torch import nn
from torch.nn import functional as F, Flatten

class double_conv(nn.Module):
    def __init__(self,in_channel,out_channel):
        super(double_conv, self).__init__()

        self.in_channel = in_channel
        self.out_channel = out_channel

        self.conv_layer_1 = nn.Conv2d(in_channel,out_channel,3,1,1,padding_mode='reflect',bias=False)
        self.conv_layer_2 = nn.Conv2d(out_channel, out_channel, 3, 1, 1, padding_mode='reflect', bias=False)
        self.norm = nn.BatchNorm2d(out_channel)
        self.drop = nn.Dropout2d(0.3)
        self.activate = nn.LeakyReLU()

    def forward(self, x):
        temp = self.conv_layer_1(x)
        temp = self.norm(temp)
        temp = self.drop(temp)
        temp = self.activate(temp)

        temp = self.conv_layer_2(temp)
        temp = self.norm(temp)
        temp = self.drop(temp)
        temp = self.activate(temp)

        return temp

class decode(nn.Module):
    def __init__(self, channel):
        super(decode, self).__init__()

        self.channel = channel

        self.conv_layer = nn.Conv2d(channel, channel, 3, 2, 1, padding_mode='reflect', bias=False)
        self.norm = nn.BatchNorm2d(channel)
        self.activate = nn.LeakyReLU()

    def forward(self,x):
        temp = self.conv_layer(x)
        temp = self.norm(temp)
        temp = self.activate(temp)

        return temp

class encode(nn.Module):
    def __init__(self,channel):
        super(encode, self).__init__()
        self.conv = nn.Conv2d(channel, channel//2, 1, 1)

    def forward(self, x, feature_map):
        temp = F.interpolate(x, scale_factor=2, mode='nearest')
        temp = self.conv(temp)
        temp = torch.cat((temp, feature_map), dim=1)

        return temp

class U_net():
    def __init__(self):
        super(U_net, self).__init__()
        # decoding part
        self.conv1 = double_conv(3, 64)
        self.decode1 = decode(64)

        self.conv2 = double_conv(64, 128)
        self.decode2 = decode(128)

        self.conv3 = double_conv(128, 256)
        self.decode3 = decode(256)

        self.conv4 = double_conv(256, 512)
        self.decode4 = decode(512)

        self.conv5 = double_conv(512, 1024)

        # encoding part
        self.encode1 = encode(1024)
        self.conv6 = double_conv(1024, 512)

        self.encode2 = encode(512)
        self.conv7 = double_conv(512, 256)

        self.encode3 = encode(256)
        self.conv8 = double_conv(256, 128)

        self.encode4 = encode(128)
        self.conv9 = double_conv(128, 64)

        # fully connected layer
        self.result = Flatten()
        self.out = nn.Linear(4194304, 1)

    def forward(self, x):
        down_1 = self.conv1(x)
        down_2 = self.conv2(self.decode1(down_1))
        down_3 = self.conv3(self.decode2(down_2))
        down_4 = self.conv4(self.decode3(down_3))
        down_5 = self.conv5(self.decode4(down_4))

        up_1 = self.conv6(self.encode1.forward(down_5, down_4))
        up_2 = self.conv7(self.encode2(up_1,down_3))
        up_3 = self.conv8(self.encode3(up_2, down_2))
        up_4 = self.conv9(self.encode4(up_3, down_1))

        results = self.out(self.result(up_4))

        return results

if __name__ == '__main__':
    x = torch.randn(2,3,256,256)
    model = U_net()
    # model.forward(x)
    print(model.forward(x).shape)


